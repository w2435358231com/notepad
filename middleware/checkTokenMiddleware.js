const jwt = require("jsonwebtoken");
const { secret } = require("../config/config");
//token 校验的路由中间件了
let checkTokenMiddleware = (req, res, next) => {
  //有token 校验token
  let token = req.get("token");
  if (!token) {
    return res.json({
      code: "10001",
      msg: "token缺失",
      data: null,
    });
  }
  jwt.verify(token, secret, (err, data) => {
    if (err) {
      return res.json({
        code: "10001",
        msg: "token校验失败",
        data: null,
      });
    }
    //保存用户信息
    req.user = data;
    next();
  });
};
module.exports = checkTokenMiddleware;
