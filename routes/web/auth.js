var express = require("express");
var router = express.Router();
const md5 = require("md5");
const UserModel = require("../../model/UserModule.js");

//首页操作
router.get("/", (req, res) => {
  // req.redirect("/account/");
  res.redirect("/account");
});

//注册操作
router.get("/reg", (req, res) => {
  res.render("auth/reg");
});

//注册操作
router.post("/reg", (req, res) => {
  // res.send(req.body);
  //页面跳转时先连接数据库操作 后在启动服务
  //对用户传过来的密码进行加密处理
  let { username, password } = req.body;
  UserModel.findOne({ username: username }).then((data) => {
    if (data) {
      res.send("用户名重复");
    } else {
      UserModel.create({ ...req.body, password: md5(password) }).then(() => {
        res.render("success", { msg: "注册成功", url: "/account" });
      });
    }
  });
});

//登录
router.get("/login", (req, res) => {
  res.render("auth/login");
});

//登录操作
router.post("/login", (req, res) => {
  let { username, password } = req.body;
  //从数据中查找  用户名和密码是否正确
  UserModel.findOne({ username: username, password: md5(password) })
    .then((data) => {
      if (data) {
        req.session.username = data.username;
        req.session._id = data._id;
        res.render("success", { msg: "登陆成功", url: "/account" });
      }
    })
    .catch(() => {
      res.status(500).send("登录失败，请稍后再试");
    });
});

//退出登录
router.get("/logout", (req, res) => {
  req.session.destroy(() => {
    res.render("success", { msg: "退出登录", url: "/login" });
  });
});

module.exports = router;
