var express = require("express");
var router = express.Router();

const AccountModel = require("../../model/AccountModle");
const moment = require("moment");
//导入路由中间件
const checkLoginMiddleware = require("../../middleware/checkLoginMiddleware.js");
//记账本的列表
router.get("/account", checkLoginMiddleware, function (req, res, next) {
  //获取所有的账单信息
  AccountModel.find()
    .sort({ time: -1 })
    .exec()
    .then((data) => {
      res.render("list", {
        accounts: data,
        moment,
      });
    });
});

//创建记录
router.get("/account/create", checkLoginMiddleware, function (req, res, next) {
  res.render("create");
});

//新增记录
router.post("/account", checkLoginMiddleware, (req, res) => {
  // console.log(req.body);
  // moment(req.body.time) //将日期字符串转为对象  在转为日期对象
  AccountModel.create({
    ...req.body,
    time: moment(req.body.time).toDate(),
  }).then((data) => {
    //成功提醒
    res.render("success", { msg: "添加成功哦~~~", url: "/account" });
  });
});

//删除记录
router.get("/account/:id", checkLoginMiddleware, (req, res) => {
  //获取 params 的 id 参数
  let id = req.params.id;
  //删除
  AccountModel.deleteOne({ _id: id }).then(() => {
    //提醒
    res.render("success", { msg: "删除成功~~~", url: "/account" });
  });
});

module.exports = router;
