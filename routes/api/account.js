var express = require("express");
var router = express.Router();
const jwt = require("jsonwebtoken");
const AccountModel = require("../../model/AccountModle");
const moment = require("moment");
const checkTokenMiddleware = require("../../middleware/checkTokenMiddleware.js");
const { secret } = require("../../config/config");
//记账本的列表
router.get("/account", checkTokenMiddleware, function (req, res, next) {
  console.log(req.user);
  //获取所有的账单信息
  AccountModel.find()
    .sort({ time: -1 })
    .exec()
    .then((data) => {
      res.render("list", {
        accounts: data,
        moment,
      });
    });
});

//新增记录
router.post("/account", checkTokenMiddleware, (req, res) => {
  // console.log(req.body);
  // moment(req.body.time) //将日期字符串转为对象  在转为日期对象
  AccountModel.create({
    ...req.body,
    time: moment(req.body.time).toDate(),
  })
    .then((data) => {
      res.json({
        code: "20000",
        msg: "读取成功",
        data: data,
      });
    })
    .catch(() => {
      res.json({
        code: "1001",
        msg: "读取失败",
        data: null,
      });
    });
});

//删除记录
router.delete("/account/:id", checkTokenMiddleware, (req, res) => {
  //获取 params 的 id 参数
  let id = req.params.id;
  //删除
  AccountModel.deleteOne({ _id: id })
    .then((data) => {
      //提醒
      res.json({
        code: "200",
        msg: "删除成功",
        data: data,
      });
    })
    .catch((error) => {
      res.json({
        code: "1003",
        msg: "删除失败",
        data: error,
      });
    });
});
//获取单个账单信息
router.get("/account/:id", checkTokenMiddleware, (req, res) => {
  let { id } = req.params;
  //查询数据库
  AccountModel.findById(id)
    .then((data) => {
      res.json({
        code: "200",
        msg: "读取成功",
        data: data,
      });
    })
    .catch((error) => {
      res.json({
        code: "10004",
        msg: "读取失败",
        data: error,
      });
    });
});
//获取单个账单信息
router.patch("/account/:id", checkTokenMiddleware, (req, res) => {
  let { id } = req.params;
  //跟新数据库
  AccountModel.updateOne({ _id: id }, req.body)
    .then(() => {
      //根据更新的id再次读取数据库中的数据
      AccountModel.findById(id)
        .then((data) => {
          res.json({
            code: "200",
            msg: "更新成功",
            data: data,
          });
        })
        .catch(() => {
          res.json({
            code: "10023",
            msg: "读取失败",
            data: null,
          });
        });
    })
    .catch((error) => {
      res.json({
        code: "10004",
        msg: "更新失败",
        data: error,
      });
    });
});
module.exports = router;
