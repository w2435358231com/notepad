var express = require("express");
var router = express.Router();
const md5 = require("md5");
const UserModel = require("../../model/UserModule.js");
const jwt = require("jsonwebtoken");
const { secret } = require("../../config/config.js");
//登录操作
router.post("/login", (req, res) => {
  let { username, password } = req.body;
  //从数据中查找  用户名和密码是否正确
  UserModel.findOne({ username: username, password: md5(password) })
    .then((data) => {
      let token = jwt.sign({ username: data.username }, secret, {
        expiresIn: 60 * 60 * 24 * 7,
      });
      if (data) {
        res.json({
          code: "200",
          msg: "登陆成功",
          data: token,
        });
      } else {
        res.json({
          code: "10000",
          msg: "登陆失败",
          data: token,
        });
      }
    })
    .catch(() => {
      res.json({
        code: "1000",
        msg: "登录失败",
        data: null,
      });
    });
});

//退出登录
router.get("/logout", (req, res) => {
  req.session.destroy(() => {
    res.render("success", { msg: "退出登录", url: "/login" });
  });
});

module.exports = router;
