var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var indexRouter = require("./routes/web/index");
const accountRouter = require("./routes/api/account.js");
const authRouter = require("./routes/web/auth.js");
const session = require("express-session");
const MongoStore = require("connect-mongo");
const { DBHOST, DBPOST, DBNAME } = require("./config/config.js");
const acountTokenRouter = require("./routes/api/accountToken.js");
var app = express();
//设置session中间件
app.use(
  session({
    name: "sid", //设置setcookie 的名字
    secret: "atguigu", //参与加密的字符串
    saveUninitialized: false, //;是否每次请求都设置一个cookie来存放session_id
    resave: true, //是否每次请求重新保存session
    store: MongoStore.create({
      mongoUrl: `mongodb://${DBHOST}:${DBPOST}/${DBNAME}`, //将session_id 存储在数据中
    }),
    cookie: {
      httpOnly: true, //  开启前端不能哦通过js获取cookie  document.cookie
      maxAge: 1000 * 30, //session_id 的过期时间
    },
  })
);
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use("/api", acountTokenRouter);
app.use("/", indexRouter);
app.use("/api", accountRouter);
app.use("/", authRouter);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.render("404");
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
