const { DBHOST, DBPOST, DBNAME } = require("../config/config.js");
const db = (success, error) => {
  if (typeof error !== "function") {
    error = () => {
      console.log("连接失败");
    };
  }
  const mongoose = require("mongoose");
  //1.连接数库
  mongoose.connect(`mongodb://${DBHOST}:${DBPOST}/${DBNAME}`);
  //2.设置回调
  mongoose.connection.once("open", () => {
    success();
  });
  mongoose.connection.once("error", () => {});
  mongoose.connection.once("close", () => {
    console.log("连接关闭");
  });
};
module.exports = db;
