const mogoose = require("mongoose");
let AccountSchema = new mogoose.Schema({
  title: {
    type: String,
    default: "买电脑",
  },
  time: Date,
  type: {
    type: Number,
    default: -1,
  },
  account: String,
  remarks: String,
});
let AccountModel = mogoose.model("Accounts", AccountSchema);
module.exports = AccountModel;
