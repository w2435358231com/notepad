const mogoose = require("mongoose");
let BookSchema = new mogoose.Schema({
  name: String,
  author: String,
  price: Number,
});
let BookModel = mogoose.model("books", BookSchema);
module.exports = BookModel;
